# Djehouty project

> A value analysis project : _gauging efficiency of projects is not the best-shared practice_.


## Aim

Analyzing efficiency, sustainability and capability to achieve a (IT) project is harder than a project
manager would expect it.

Mainly, aligning the objectives of a CIO with those of its internal clients is the keystone of
the analysis. If a CIO wishes to optimize the resources at their disposal to be able to respond
quickly to the largest number of customers, the business departments do not want to worry about
the internal costs of their request. The result is therefore often unsatisfactory projects because
the expectations and issues have not been evaluated to succeed; it's a fool's bargain.
From the early beginning of the projects, at no time did the two shareholders of the project
take five real little minutes - a little more ... - figure together out implications of their
project. This is the purpose of the value analysis.

The value analysis results from both the qualitative assessment of the business objectives,
the organization and the functions requested, and quantitative elements such as the profitability
of the project. By combining these confronted evaluations with the business departments,
you obtain a shared value of a project.
Since the early 2000, the French government has proposed a value analysis method for its own
IS projects. [MAREVA 2](http://references.modernisation.gouv.fr/mareva-2) is implemented
in x-axis value analysis.  

But this method gives us only the shared value of an IT project, it never evaluates risks of your
project. To have the sustainability of the project, it is necessary to measure the risks with regard
to the value of a project. It is not a matter of deciding that a project is going to be done, or not,
but of knowing what to implement to ensure that shared objectives are achieved.
The y-axis is dedicated to the global risk evaluation of the project.

At the end of the day, the project is represented in a coordinate plan (value analysis, risk analysis) 
and compare to other projects, it is easier to plan projects to come.

## Model

Value Analysis model is brought to light in the [project's documents](docs/home.md).

## Project Lifecycle

Here are some of the rules of project lifecycle management.

### Release cycle

As in [Ubuntu release cycle](https://ubuntu.com/about/release-cycle), version is based on year and month - with nonames -
following by a major release number and a minor release number :

* major release numbers indicates major evolution with no allowed backturn
* minor release numbers indicates patch or minor modification which implies
no real changes or adds.

        Ex. 2020.12.02.12

### Contributing

A pull request, actually under GitLab a merge-request, is still the best way to provide a bug fix or to propose
enhancements to Djehouty. Proposing a change is described in [CONTRIBUTING](CONTRIBUTING.md) file.

### Lifecycle management
[changelog](CHANGELOG.md)


