
_The following copyright statements and licenses apply to various open
source software components (or portions thereof) that are distributed with
Djehouty._

_Djehouty may also include other components, which may contain additional open
source software packages. One or more such open_source_licenses.txt files may
therefore accompany these components._

_The following is a listing of the open source components detailed in this
document.  This list is provided for your convenience; please read further if
you wish to review the copyright notice(s) and the full text of the license
associated with each component._

---
## Python Language

Python is under [PSFL](https://docs.python.org/3/license.html)

## Django Framework

Django Framework is under [BSD 3-clauses](https://opensource.org/licenses/BSD-3-Clause)
