# Proposing a change

Under Gitlab,  merge-request is still the best way to provide a bug fix or to propose
enhancements to Djehouty.

## Checking issues and merge requests

Before working on a change, you should check to see if someone else also raised
the topic or started adressing it on a merge-request by searching on
[Djehouty Gitlab issues](https://gitlab.com/open-works/djehouty/issues).

## Step up your environment

### Stoftware stack

Before working on this project, setup a friendly environment with the
following components :

* [PyCharm Community](https://www.jetbrains.com/pycharm/download/#section=linux) with [Python](https://www.python.org/downloads/)
* [Django framework](https://www.djangoproject.com/download/)
  * [django-import-export](https://django-import-export.readthedocs.io/en/latest/index.html)
* [Gitlab repository](https://gitlab.com/open-works/djehouty)

Djehouty repository is designed for this software stack. A ``.gitignore`` file is issued to limit the saving of files
useful for IDEs but useless for the project.

Documents are mainly written with [Markdown](https://daringfireball.net/projects/markdown/)

* Data models are drawn with [Yed](https://www.yworks.com/products/yed)
* images in SVG or PNG format are made with [Inkscape](https://inkscape.org/)

### Git and Gitlab

First, you have to configure your git with global variables of your developpers name and
email

```bash
$ git config --global user.name "Your name"
$ git config --global user.email you@site.org
```  

If you are new to git we recommend reading [the git book](https://git-scm.com/book/en/v2)

### Get the code

1. Create a [Gitlab account](https://gitlab.com/users/sign_up)
   * then, [sign in](https://gitlab.com/users/sign_in)
   * we recommend that you configure 2FA
   * we recommend that you configure SSH key
2. Djehouty repository is clonable

   Using HTTPS
   ````bash
   $ git clone https://gitlab.com/open-works/djehouty.git
   ````
   Using SSH
   ````bash
   $ git clone git@gitlab.com:open-works/djehouty.git
   ````
   If you are using SSH mode check carefully before you
   have properly set up your environment to load your SSH keys
   
## Work on your merge request

1. Check [issues list](https://gitlab.com/open-works/djehouty/issues?scope=all&utf8=%E2%9C%93&state=all)
2. Check opened [merge resquests](https://gitlab.com/open-works/djehouty/-/merge_requests)
3. Create a new branch
   * Using PyCharm
     1. Menu VCS > Git > Branches...
     2. + New Branch
   * Using command line with SSH
     ````bash
     $ git remote add <branch> git@gitlab.com:open-works/djehouty.git   
     ````
4. Make your developments
5. Create a new [merge requests](https://gitlab.com/open-works/djehouty/-/merge_requests)


