# Data Structures

>In order to simplify data management in queries and evaluations, 
use of Arrays should be required. By using PostgreSQL Databases,
we are able to implement Arrays in databases.
> 
>This feature from PostgreSQL is helpful to model Evaluations
and Thresholds.

Here are short notes on data structures in models.

## Global dependencies

![Model](images/global.svg)

## About languages and requests

### Countries and Languages

![languages](images/data_languages.svg)

Country
* id (internal primary key) : integer
* iso_3166_a2 : char[2]
* iso_3166_a3 : char[3]
* iso_3166_n3 : integer
* label_f : char[200]
* label_e : char[200]

Language
* id (internal primary key) : integer
* iso-ietf-tag : char[24]
* label_f : char[200]
* label_e : char[200]
* label_o : char[200], Null  
* Country : Country

### Queries

![queries](images/data_query.svg)

#### Criteria :

> Criteria are cosmetics and are simply used to publish axes

* id (internal primary key) : integer
* language
* label

#### EvaluationSet :

> EvaluationSet is a registering table or class of allowed answers and value for each of
> these answers. This will be used in the process calculus of queries value. 

* id (internal primary key) : integer
* language : Language
* answer : array of 4 char[255]
* value : array of 4 integer


#### Query :

> A query is one of the foundation of the value model. This class is recursive and manage a
> simple query as well as a group query.
> 
> Each query has an EvaluationSet to offer at the most 4 possible answers and associated values.
> Each query is a member of a group of queries (or query set). If the query is a query group
> (that is to say, is referenced as au query_group for another one), it will require a Threshold
> for the calculs of the group value.
> 
> Algorithm will be :
<pre>Value = Sum of subgroup queries value
    for each threshold_value in Threshold[0,1,2,3]
        if Value < threshold_value : Value = threshold_value
        break</pre>

* id (internal primary key) : integer
* language
* label
* criteria
* query_id
* query_group
* answer
* threshold_group
  


