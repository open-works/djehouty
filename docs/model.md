# Value Analysis

MAREVA - stands for Méthode d'Analyse et de Remontée de la Valeur (Analysis and Probing Method of Value) - is a method for evaluating business and economic values as a result of IS projects during the different steps of their life cycle.

These values allow trade-offs between different product development scenarios. MAREVA 2 thus offers the opportunity to pilot the construction of a product with regard to the gains and the complete costs that it entails.

## Synopsis

This method is a process to steer the performance of enterprise IT projects portfolio. It collects data from multiple objectives analysis :

* Business objectives
  * User experience
  * Digital transformation
  * Enterprise image and quality of consumer relation
  * Regulation
  * Policies alignment
* IT Objectives
  * Software obsolescence and applications legacy
  * Security
  * Enterprise architecture alignment

Both of this objectives are collected in a *strategic analysis* folder

* Costs
  * Business costs such as those due to impact of each project scenario on the transformation of business processes
  * IT costs
    * Build and run costs
    * impact of each project scenario on the transformation of IT processes

This analysis is collected in a *profitability analysis* folder

## Business objectives

### User experience

Since users experience is a key to change adoption, business objectives have to take into account this subject.
In this them questions are about the number of users, advantages and services improvement.

### Digital transformation

This subject is about what does Digital Transformation bing out and how it does improve business objectives

### Enterprise image and quality of consumer relation

Sponsorship is mainly leading by enterprise image and consumer relationship. Thus, these subjects are part of the
questions which would improve the commercial objectives and entice business teams

### Regulation

Even if regulations seem to be constraints, each part of an information system project has to be compliant with.
The key to overwhelm these constraints is to include them as soon as possible and, almost, before the beginning
of the project.

### Policies alignment

As with regulation, internal policies must be followed so as not to generate opposition.
Plus, it's a great foundation when you've given your word ... to keep it

## IT objectives

### Software obsolescence and applications legacy

The more your application is obsolete, the more it will cost or it will need engineer to maintain it.
An obsolete IT is CIO's nightmare.

### Security

Security by design is a keyword for tranquility.

### Enterprise architecture alignment

Keep it simple and smart. The more your architecture is complex, the most difficulties you will have to improve it.

## Costs

Costs are a way to consider your software during long times. Usually everybody take into account investments and above
all initial investment. But, the cost of an information system, is the time your business involve in using it, is the
time yours teams take to run and maintain software, is the cost of supports and this is summable for five or ten years.