# ![Djehouty](images/dejouhty.png "Djehouty or Thot from wikipedia") Djehouty project 

> A value analysis project : _gauging efficiency of projects is not the best-shared practice_.


## Aim

Analyzing efficiency, sustainability and capability to achieve a (IT) project is harder than a project
manager would expect it.

Mainly, aligning the objectives of a CIO with those of its internal clients is the keystone of
the analysis. If a CIO wishes to optimize the resources at their disposal to be able to respond
quickly to the largest number of customers, the business departments do not want to worry about
the internal costs of their request. The result is therefore often unsatisfactory projects because
the expectations and issues have not been evaluated to succeed; it's a fool's bargain.
From the early beginning of the projects, at no time did the two shareholders of the project
take five real little minutes - a little more ... - figure together out implications of their
project. This is the purpose of the value analysis.

The value analysis results from both the qualitative assessment of the business objectives,
the organization and the functions requested, and quantitative elements such as the profitability
of the project. By combining these confronted evaluations with the business departments,
you obtain a shared value of a project.
Since the early 2000, the French government has proposed a value analysis method for its own
IS projects. [MAREVA 2](http://references.modernisation.gouv.fr/mareva-2) is implemented
in x-axis value analysis.  

But this method gives us only the shared value of an IT project, it never evaluates risks of your
project. To have the sustainability of the project, it is necessary to measure the risks with regard
to the value of a project. It is not a matter of deciding that a project is going to be done, or not,
but of knowing what to implement to ensure that shared objectives are achieved.
The y-axis is dedicated to the global risk evaluation of the project.



## Analysis

A project is represented in a coordinate plan (value analysis, risk analysis) 
and compare to other projects, it is easier to plan projects to come. In this
plan, four zones can be identified :

 * Q-area : the quagmire, in this area you should not involve your teams in such projects this is too risky and there is no profits
 * A-area : this area is safe regarding project risk analysis however the project value is less interesting than it could be. In that case, optimize project value would be an improvement
 * S-area : the safe area, your projects are well managed and sure enough to be conducted
 * R-area : since the value of the project is good enough to be done, an improvement of risk control must be applied to secure projet lifecycle

![QuASaR](images/QuASaR.png "QuASaR quadrant")

In this QuASaR quadrant, each portfolio projects is quantified and thus both IT and business is able to tag a priority to these projects and applied project method to update value or risk control in order to optimize it.

## Model

### Value Analysis - X-axis

[Value analysis model](model.md) used is based on the French Government process to analyse value of a project.

#### *Strategic* model

Strategic model is based on qualitative questions for which a weighting is assigned to each allowed response. These questions are grouped by **themes** and then by **axis** of analysis. Weighting levels (for instance *low*, *medium* and *high*) are assigned to each theme or axis. Value of these levels is estimated using the sum of the values ​​of the answers to each question in the theme.

#### *Cost* model  

### Risk Analysis - Y-axis